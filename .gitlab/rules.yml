###############################################################################
# Rules                                                                       #
###############################################################################
# Run on merge requests to this project
.run_on_merge_request:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
      variables:
        S3_BUCKET_RELEASES: "${S3_BUCKET_STAGING}"
        S3_BUCKET_RELEASES_ARCHIVE: "${S3_BUCKET_RELEASES_ARCHIVE_STAGING}"

# Run on merge request to an external project (sample-images, create-osbuild, etc) that
# triggers this pipeline with a trigger job
.run_on_external_merge_request:
      # Skip the promote product job and the ones that depends on it
    - if: '$CI_PIPELINE_SOURCE == "pipeline" && $UPSTREAM_PIPELINE_SOURCE == "merge_request_event" && $CI_JOB_NAME =~ /.*-product-.*/'
      when: never
      # Don't build the following images: raw, non-sample, sample or osbuildvm
    - if: '$CI_PIPELINE_SOURCE == "pipeline" && $UPSTREAM_PIPELINE_SOURCE == "merge_request_event" && $CI_JOB_NAME =~ /^build-(raw|non-sample|sample|osbuildvm)-images.*/'
      when: never
      # Don't build or test the update OSTree repo. It doesn't support build against the nightly
    - if: '$CI_PIPELINE_SOURCE == "pipeline" && $UPSTREAM_PIPELINE_SOURCE == "merge_request_event" && $CI_JOB_NAME =~ /.*-update-test.*/'
      when: never
      # When the MR comes from a Pungi config repo, just set these variables
    - if: '$CI_PIPELINE_SOURCE == "pipeline" && $UPSTREAM_PIPELINE_SOURCE == "merge_request_event" && $UPSTREAM_PROJECT == $PUNGI_CONFIG_PROJECT'
      variables:
        S3_BUCKET_RELEASES: "${S3_BUCKET_STAGING}"
        S3_BUCKET_RELEASES_ARCHIVE: "${S3_BUCKET_RELEASES_ARCHIVE_STAGING}"
      # Skip the upload report jobs
    - if: '$CI_PIPELINE_SOURCE == "pipeline" && $UPSTREAM_PIPELINE_SOURCE == "merge_request_event" && $CI_JOB_NAME =~ /^upload-.*report.*/'
      when: never
      # When the MR came from other projects and the job is not one of the ablove, just set these variables
    - if: '$CI_PIPELINE_SOURCE == "pipeline" && $UPSTREAM_PIPELINE_SOURCE == "merge_request_event"'
      variables:
        S3_BUCKET_RELEASES: "${S3_BUCKET_STAGING}"
        S3_BUCKET_RELEASES_ARCHIVE: "${S3_BUCKET_RELEASES_ARCHIVE_STAGING}"
        BASE_URL: "${NIGHTLY_COMPOSE}"

# Run post-merge on merges to the default branch of the lockfile branch
.run_on_merge_to_compose_config:
    - if: '$CI_PIPELINE_SOURCE == "pipeline" && $UPSTREAM_PIPELINE_SOURCE == "push" && $UPSTREAM_PROJECT == $PUNGI_CONFIG_PROJECT && $UPSTREAM_BRANCH == $UPSTREAM_DEFAULT_BRANCH'

# Run when tags are created on the lockfile project
.run_on_release_tag:
    - if: '$CI_PIPELINE_SOURCE == "pipeline" && $UPSTREAM_PIPELINE_SOURCE == "push" && $UPSTREAM_PROJECT == $PUNGI_CONFIG_PROJECT && $UPSTREAM_COMMIT_TAG'
      variables:
        RELEASE: $UPSTREAM_COMMIT_TAG

# Run post-merge  on merge to the default branch of this project
.run_on_merge:
    - if: '$CI_PIPELINE_SOURCE == "push" && $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH'

# Run for scheduled pipeline triggers
.run_on_schedule:
    - if: '$CI_PIPELINE_SOURCE == "schedule"'
      variables:
        RELEASE: 'nightly'

# Run when triggered from Gitlab's web UI
.run_on_web_ui_trigger:
  - if: '$CI_PIPELINE_SOURCE == "web"'

# Run when triggered by a trigger job
.run_on_multi_project_pipeline_trigger:
  - if: '$CI_PIPELINE_SOURCE == "pipeline"'

# Run when triggered using the Gitlab API
.run_on_api_trigger:
  - if: '$CI_PIPELINE_SOURCE == "trigger"'

.run_always:
  - !reference [.run_on_external_merge_request]
  - !reference [.run_on_merge_request]
  - !reference [.run_on_merge]
  - !reference [.run_on_merge_to_compose_config]
  - !reference [.run_on_release_tag]
  - !reference [.run_on_multi_project_pipeline_trigger]
  - !reference [.run_on_schedule]
  - !reference [.run_on_web_ui_trigger]
  - !reference [.run_on_api_trigger]

.run_on_daily_schedule:
  if: $CI_PIPELINE_SOURCE == "schedule" && $JOB_FREQUENCY == "daily"
